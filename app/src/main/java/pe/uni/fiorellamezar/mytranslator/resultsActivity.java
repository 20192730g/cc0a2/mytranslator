package pe.uni.fiorellamezar.mytranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import com.google.mlkit.common.model.DownloadConditions;
import com.google.mlkit.nl.translate.TranslateLanguage;
import com.google.mlkit.nl.translate.Translation;
import com.google.mlkit.nl.translate.Translator;
import com.google.mlkit.nl.translate.TranslatorOptions;

public class resultsActivity extends AppCompatActivity {
    TextView textViewExtract;
    TextView textViewTranslated;

    //Create an English-Spanish translator
    TranslatorOptions options =
            new TranslatorOptions.Builder()
                    .setSourceLanguage(TranslateLanguage.ENGLISH)
                    .setTargetLanguage(TranslateLanguage.SPANISH)
                    .build();

    final Translator englishSpanishTranslator = Translation.getClient(options);

    DownloadConditions conditions = new DownloadConditions.Builder()
            .requireWifi()
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        textViewExtract = findViewById(R.id.text_view_extract);
        textViewTranslated = findViewById(R.id.text_view_translated);

        Intent intent = getIntent();

        String sCaptured = intent.getStringExtra("TEXT-CAPTURED");

        textViewExtract.setText(sCaptured);

        englishSpanishTranslator.downloadModelIfNeeded(conditions)
                .addOnSuccessListener(v1 -> {

                })
                .addOnFailureListener(e -> Toast.makeText(getApplicationContext(), R.string.toast_download_model_error, Toast.LENGTH_SHORT).show());

        englishSpanishTranslator.translate(sCaptured)
                    .addOnSuccessListener(s ->
                            textViewTranslated.setText(s)
                    ).addOnFailureListener(e -> Toast.makeText(getApplicationContext(), R.string.toast_translate_error, Toast.LENGTH_SHORT).show());

    }
}