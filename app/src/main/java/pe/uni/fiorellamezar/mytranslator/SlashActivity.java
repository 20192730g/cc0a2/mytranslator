package pe.uni.fiorellamezar.mytranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SlashActivity extends AppCompatActivity {
    ImageView imageViewSlash;
    Animation animationImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slash);

        imageViewSlash = findViewById(R.id.image_view_splash);
        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);

        imageViewSlash.setAnimation(animationImage);

        new CountDownTimer(4000, 1000){
            @Override
            public void onTick(long millisUntilFinished){

            }

            @Override
            public void onFinish() {
                Intent intent= new Intent(SlashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();

    }
}