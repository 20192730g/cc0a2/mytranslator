package pe.uni.fiorellamezar.mytranslator;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.common.util.concurrent.ListenableFuture;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

public class TranslatorActivity extends AppCompatActivity {
    Button buttonCapture, buttonTranslate;
    PreviewView previewView;

    //Camera X
    ImageCapture imageCapture;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;

    //Reconocedor de texto
    TextRecognizer recognizer;
    Uri uriPhoto;
    Task<Text> result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator);

        buttonCapture = findViewById(R.id.button_take_photo);
        previewView = findViewById(R.id.preview_view);
        buttonTranslate = findViewById(R.id.button_translate);

        Intent intent = new Intent(TranslatorActivity.this, resultsActivity.class);

        //Camera X
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(()->{
            try {
                // Se utiliza para vincular el ciclo de vida de las cámaras al propietario del ciclo de vida
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                startCameraX(cameraProvider);
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }, getExecutor());

        //Reconocedor de texto
        recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);

        //Botón para capturar imagen
        buttonCapture.setOnClickListener(v -> {
            if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            }else{
                ActivityCompat.requestPermissions(TranslatorActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
            }

        });


        buttonTranslate.setOnClickListener(v -> {
            InputImage inputImage;
            if(uriPhoto!=null){
                try {
                    inputImage = InputImage.fromFilePath(this, uriPhoto);
                    result = recognizer.process(inputImage)
                                            .addOnSuccessListener(visionText -> {
                                                intent.putExtra("TEXT-CAPTURED", visionText.getText());
                                                startActivity(intent);
                                                Toast.makeText(getApplicationContext(), R.string.text_view_msg_succesfuly, Toast.LENGTH_SHORT).show();
                                            })
                                            .addOnFailureListener(
                                                    e -> Toast.makeText(getApplicationContext(), R.string.msg_toast, Toast.LENGTH_SHORT).show());

                } catch (IOException e) {
                    e.printStackTrace();
                }
                uriPhoto = null;
            }else{
                Toast.makeText(getApplicationContext(), R.string.toast_image_null, Toast.LENGTH_SHORT).show();
            }

        });

    }

    /////////////////////// CAMERA X////////////////////////////
    private void startCameraX(ProcessCameraProvider cameraProvider) {

        //Preview
        Preview preview = new Preview.Builder().build();
        preview.setSurfaceProvider(previewView.getSurfaceProvider());

        //Configuración de la cámara
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        cameraProvider.unbindAll(); //Desvincula todos los casos de uso para volver a vincular

        imageCapture = new ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build();

        cameraProvider.bindToLifecycle(this, cameraSelector, preview,imageCapture);
    }

    private Executor getExecutor(){
        return ContextCompat.getMainExecutor(this);
    }

    private void takePhoto(){
        // Crear nombre con marca de tiempo y entrada de MediaStore.
        long timestamp = System.currentTimeMillis();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, timestamp);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");

        // Configure el oyente de captura de imágenes, que se activa después
        // de que se haya tomado la foto
        imageCapture.takePicture(
                // Crear objeto "outputOptions" que contiene archivo + metadatos
                new ImageCapture.OutputFileOptions.Builder(
                        getContentResolver(),
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        contentValues
                ).build(),
                getExecutor(),
                new ImageCapture.OnImageSavedCallback() {
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                        uriPhoto = outputFileResults.getSavedUri();
                        Toast.makeText(TranslatorActivity.this, R.string.toast_msg_photo_successfully, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(@NonNull ImageCaptureException exception) {
                        Toast.makeText(TranslatorActivity.this, R.string.toast_msg_photo_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}

