package pe.uni.fiorellamezar.mytranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonStart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = findViewById(R.id.button_start);

        buttonStart.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, TranslatorActivity.class);
            startActivity(intent);
        });
    }

}